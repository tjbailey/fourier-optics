# FTOptics (Fourier Transform Optics)

## What is FTOptics?
FTOptics is a simple python library to simulate classical wave optics, using
the Fourier optic propagation technique.

```
from ftoptics import AmplitudeField
LightBeam = AmplitudeField(size=(1024,1024))
LightBeam.generate_gaussian(sigma=10)
LightBeam.propagate(100)
LightBeam.plot(type='Intensity', show=True)
```

[INCLUDE IMAGE GENRATED BY ABOVE CODE]

## Features
- Model effects of common optics elements such as lenses or waveplates
- Integrates easily with common libraries such as numpy and matplotlib
- Easy to work with scalar or vector (polarization) fields
- Transparent and well documented implementation of physics to code
- Support for JIT and parrallel computation with numba
- Support for CUDA using cupy

## Installation
- Guide coming soon!

## Source
Source code can be found at
[https://gitlab.com/tjbailey/fourier-optics](https://gitlab.com/tjbailey/fourier-optics)

## License
The project is licensed under the MIT license.
