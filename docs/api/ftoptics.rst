FTOptics
========

Classes
-------

.. toctree::
  :maxdepth: 1

  ftoptics.AmplitudeField

Methods
-------

.. toctree::
  :maxdepth: 1

  ftoptics.AmplitudeField.propagate
  ftoptics.AmplitudeField.lens
  ftoptics.AmplitudeField.cylindrical_lens
  ftoptics.AmplitudeField.aperture
  ftoptics.AmplitudeField.slit
