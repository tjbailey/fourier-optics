.. FTOptics documentation master file, created by
   sphinx-quickstart on Tue Jul 20 12:38:16 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FTOptics's documentation!
====================================

.. toctree::
   :maxdepth: 2

   getting_started
   tutorials
   api/api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
