"""
The default module for the ftoptics package.

This module contains the version of classes that use standard single threaded
numpy functions.
"""

import numpy as np
import numpy.fft as fft
import matplotlib.pyplot as plt


class AmplitudeField:
    """
    Array with methods to perform optical operations.

    Attributes
    ----------
    array: ndarray
        Two dimensional array containing the complex amplitudes of the field.
    x_size: int
        The size of the array in the x-direction.
    y_size: int
        The size of the array in the y-direction.
    scale: float
        The size of each array element in length units
        (eg. scale=1e-3 if each array element corresponds to 1 millimeter).
    k: float
        The magnitude of the light's wavevector. k = 2*pi/wavelength.

    Methods
    -------
    propagate
        Propagates the field through free space.
    lens
        Applies the phase change created by a thin 'spherical' lens.
    cylindrical_lens
        Applies the phase change created by a thin 'cylindrical' lens.
    aperture
        Applies the effect of a circular aperture.
    slit
        Applites the effect of a single slit.
    """

    def __init__(self, array=None, size=None, scale=None, resolution=None,
                 precision='double', wavelength=6.33e-7):
        """
        Parameters
        ----------
        array: ndarray or None, optional, default: None
            A two dimensional array that is used to initialize the amplitude.
            If None then the size parameter will define an array of zeros.
        size: tuple of ints or None, optional, default: None
            Defines the shape (x,y) of the starting array if array paramater
            is None.
        scale: float or None, optional, default: None
            The size of each array element in length units. If None chosen then
            this will be determined from the resolution paramater or if that is
            also None, set to 1.
        resolution: float or None, optional, default: None
            The number of array elements per length unit. Only used if scale is
            None.
        precision: {'single', 'double', 'longdouble'}, optional, default: 'double'
            The precision that each array element is stored with.
        wavelength: float, optional, default: 6.33e-7
            The wavelength of the light.
        """
        # Determine the data type to use to store the values in the array.
        if precision == 'single':
            precision = np.csingle
        elif precision == 'double':
            precision = np.cdouble
        elif precision == 'longdouble':
            precision = np.clongdouble
        # Create either blank array or array of correct data type.
        if array is not None:
            self.array = array.astype(precision)
        else:
            if size is not None:
                # Flip size tuple so rows correspond to y and columns to x
                self.array = np.zeros(size[::-1], dtype=precision)
            else:
                raise Exception('Either of the arguments array or size need'
                                ' to not be None.')
        # Find size of array.
        self.y_size, self.x_size = self.array.shape
        # Set scale of field.
        if scale is not None:
            self.scale = scale
        else:
            if resolution is not None:
                self.scale = 1/resolution
            else:
                self.scale = 1
        # Find wavevector.
        self.k = 2*np.pi/wavelength
        return None

    def propagate(self, length,  copy=False):
        """
        Find the field after propagating through free space.

        Parameters
        ----------
        length: float
            The distance the field propagates through free space.
        copy: bool, optional, default: False
            If True the instance of LightField that this function is called
            on is not modified and the function returns a copy after
            propagating. If False the instance is modified directly.

        Returns
        -------
        output_array, optional
            A copy of the instance being acted upon after propagation.

        Notes
        -----
        This method uses a Fourier optics approach to find how the beam
        propagates [1]_ [2]_.

        This is done by finding the Fourier transform of the amplitude,
        applying a transfer function and then taking the inverse Fourier
        transform to find the field after propagation.

        A scalar wave with complex amplitude :math:`E(x, y, z)` has the
        Fourier transform:

        .. math::
            \\tilde{E}(k_x, k_y, z) = \\int E(x, y, z)
            e^{i (k_x x + k_y y)} dx dy

        The propagation through free space is simple in this fourier space,
        corresponding only to a multiplication by a transfer function
        (:math:`H(k_x, k_y, d`).

        .. math::
            \\tilde{E}(k_x, k_y, z + d) = \\tilde{E}(k_x, k_y, z)
            \\times H(k_x, k_y, d)

        The transfer function is:

        .. math::
            H(k_x, k_y, d) = e^{-i d \\sqrt{k^2 - k_x^2 - k_y^2}}
            = e^{-i d \\sqrt{\\frac{4 \\pi^2}{\\lambda^2} - k_x^2 - k_y^2}}

        The field at a point :math:`z+d` can then be simply found by applying
        an inverse fourier transform to :math:`\\tilde{E}(k_x, k_y, z + d)`

        .. [1] B. E. A. Saleh & M. C. Teich, Fundamentals of Photonics,
            Chapter 4.1
        .. [2] www.rp-photonics.com/fourier_optics
        """
        if copy:
            output_array = fft.fft2(self.array)
            # Calculate the k-space values of each cell in the FT array
            k_x = fft.fftfreq(self.x_size, d=self.scale)*2*np.pi
            k_y = fft.fftfreq(self.y_size, d=self.scale)*2*np.pi
            kxs, kys = np.meshgrid(k_x, k_y)
            # Transfer function is a phase in k-space.
            H = np.exp(-1j * length * np.sqrt(self.k**2 - kxs**2 - kys**2))
            output_array = output_array * H
            output_array = fft.ifft2(output_array)
            output_array = AmplitudeField(output_array, scale=self.scale,
                                          precision=type(self.array[0, 0]),
                                          wavelength=2*np.pi/self.k)
            return output_array

        else:
            self.array = fft.fft2(self.array)
            # Calculate the k-space values of each cell in the FT array
            k_x = fft.fftfreq(self.x_size, d=self.scale)*2*np.pi
            k_y = fft.fftfreq(self.y_size, d=self.scale)*2*np.pi
            kxs, kys = np.meshgrid(k_x, k_y, sparse=True)
            # Transfer function is a phase in k-space
            self.array = (self.array * np.exp(-1j*length
                          * np.sqrt(self.k**2 - kxs**2 - kys**2)))
            self.array = fft.ifft2(self.array)
            return None

    def lens(self, focal_length, optic_axis=None, copy=False):
        """
        Apply the phase profile of a spherical lens

        Parameters
        ----------
        focal_length: float
            The focal_length of the lens.
        optic_axis: tuple of ints or None, optional, default: None
            A tuple (x,y) defining the array element which is on the optic
            axis. If None then the center of the array is used for the optic
            axis.
        copy: bool, optional, default: False
            If True the instance of LightField that this function is called
            on is not modified and the function returns a copy after
            propagating. If False the instance is modified directly.

        Returns
        -------
        output_array, optional
            A copy of the instance being acted upon after propagation.

        Notes
        -----
        The effect of a thin lens can be considered to impart a phase shift in
        real space [1]_.

        .. math::
            T(x, y) = h_0 e^{i k \\frac{x^2 + y^2}{2f}}

        The constant global phase :math:`h_0` is neglected here as it has no
        effect on a single beam. This profile is making use of the small angle
        approximation so that the profile of a spherical lens is taken to be
        the same as a parabola.

        .. [1] B. E. A. Saleh & M. C. Teich, Fundamentals of Photonics,
            Chapter 2.4
        """
        # If optic axis not given find center of the array.
        if optic_axis is None:
            optic_axis = (int(self.x_size/2), int(self.y_size/2))

        if copy:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - optic_axis[0])*self.scale
            y = (np.array(range(self.y_size)) - optic_axis[1])*self.scale
            xs, ys = np.meshgrid(x, y)
            # Transfer function is a phase in real space.
            T = np.exp(1j * self.k * (xs**2 + ys**2) / (2 * focal_length))
            output_array = self.array * T
            output_array = AmplitudeField(output_array, scale=self.scale,
                                          precision=type(self.array[0, 0]),
                                          wavelength=2*np.pi/self.k)
            return output_array

        else:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - optic_axis[0])*self.scale
            y = (np.array(range(self.y_size)) - optic_axis[1])*self.scale
            xs, ys = np.meshgrid(x, y, sparse=True)
            # Transfer function is a phase in real space.
            self.array = self.array * np.exp(1j * self.k * (xs**2 + ys**2) / (2 * focal_length))
            return None

    def cylindrical_lens(self, focal_length, rotation=0,
                         optic_axis=None, copy=False):
        """
        Apply the phase profile of a cylindrical lens

        Parameters
        ----------
        focal_length: float
            The focal_length of the lens.
        rotation: float, optional, default: 0
            The rotation [rad] of the cylindrical lens about optic axis.
            0 rotation aligned to bring a beam to a focus-line aligned along
            the y-direction.
        optic_axis: tuple of ints or None, optional, default: None
            A tuple (x,y) defining the array element which is on the optic
            axis. If None then the center of the array is used for the optic
            axis.
        copy: bool, optional, default: False
            If True the instance of LightField that this function is called
            on is not modified and the function returns a copy after
            propagating. If False the instance is modified directly.

        Returns
        -------
        output_array, optional
            A copy of the instance being acted upon after propagation.

        Notes
        -----
        The effect of a thin lens can be considered to impart a phase shift in
        real space [1]_. For a cylindrical lens this phase is uniform in one
        direction (and hence the beam will not be deflected in this direction).

        .. math::
            T(x, y) = h_0 e^{i k \\frac{x^2}{2f}}

        The constant global phase :math:`h_0` is neglected here as it has no
        effect on a single beam. This profile is making use of the small angle
        approximation so that the profile of a cylindrical lens is taken to be
        the same as a parabola.

        .. [1] B. E. A. Saleh & M. C. Teich, Fundamentals of Photonics,
            Chapter 2.4
        """
        # If optic axis not given find center of the array.
        if optic_axis is None:
            optic_axis = (int(self.x_size/2), int(self.y_size/2))

        if copy:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - optic_axis[0])*self.scale
            y = (np.array(range(self.y_size)) - optic_axis[1])*self.scale
            xs, ys = np.meshgrid(x, y)
            # Transfer function is a phase in real space.
            T = np.exp(1j * self.k
                       * ((xs*np.cos(rotation) + ys*np.sin(rotation))**2)
                       / (2 * focal_length))
            output_array = self.array * T
            output_array = AmplitudeField(output_array, scale=self.scale,
                                          precision=type(self.array[0, 0]),
                                          wavelength=2*np.pi/self.k)
            return output_array

        else:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - optic_axis[0])*self.scale
            y = (np.array(range(self.y_size)) - optic_axis[1])*self.scale
            xs, ys = np.meshgrid(x, y, sparse=True)
            self.array = self.array * np.exp(1j * self.k
                       * ((xs*np.cos(rotation) + ys*np.sin(rotation))**2)
                       / (2 * focal_length))
            return None

    def aperture(self, radius, center=None, copy=False):
        """
        Apply the effect of a circular aperture.

        Parameters
        ----------
        radius: float
            The radius of the circular aperture.
        center: tuple of ints or None, optional, default: None
            A tuple (x,y) defining the array element which is at the center
            of the aperture. If None then the center of the array is used.
        copy: bool, optional, default: False
            If True the instance of LightField that this function is called
            on is not modified and the function returns a copy after
            propagating. If False the instance is modified directly.

        Returns
        -------
        output_array, optional
            A copy of the instance being acted upon after propagation.
        """
        # If optic axis not given find center of the array.
        if center is None:
            center = (int(self.x_size/2), int(self.y_size/2))

        if copy:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - center[0])*self.scale
            y = (np.array(range(self.y_size)) - center[1])*self.scale
            xs, ys = np.meshgrid(x, y)
            # circular amplitude mask
            T = np.where((xs**2+ys**2) < (radius**2), 1, 0)
            output_array = self.array * T
            output_array = AmplitudeField(output_array, scale=self.scale,
                                          precision=type(self.array[0, 0]),
                                          wavelength=2*np.pi/self.k)
            return output_array

        else:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - center[0])*self.scale
            y = (np.array(range(self.y_size)) - center[1])*self.scale
            xs, ys = np.meshgrid(x, y)
            # circular amplitude mask
            T = np.where((xs**2+ys**2) < (radius**2), 1, 0)
            self.array = self.array * T
            return None

    def slit(self, width, center=None, rotation=0, copy=False):
        """
        Apply the effect of a slit.

        Parameters
        ----------
        width: float
            The width of the slit.
        center: tuple of ints or None, optional, default: None
            A tuple (x,y) defining the array element which is at the center
            of the slit. If None then the center of the array is used.
        rotation: float, optional, default: 0
            The angle that the slit makes to the vertical in radians. Positive
            values correspond to anti-clockwise rotation.
        copy: bool, optional, default: False
            If True the instance of LightField that this function is called
            on is not modified and the function returns a copy after
            propagating. If False the instance is modified directly.

        Returns
        -------
        output_array, optional
            A copy of the instance being acted upon after propagation.
        """
        # If optic axis not given find center of the array.
        if center is None:
            center = (int(self.x_size/2), int(self.y_size/2))

        if copy:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - center[0])*self.scale
            y = (np.array(range(self.y_size)) - center[1])*self.scale
            xs, ys = np.meshgrid(x, y)
            # Slit amplitude mask.
            T = np.where(abs(xs*np.cos(rotation) - ys*np.sin(rotation))
                         < width/2, 1, 0)
            output_array = self.array * T
            output_array = AmplitudeField(output_array, scale=self.scale,
                                          precision=type(self.array[0, 0]),
                                          wavelength=2*np.pi/self.k)
            return output_array

        else:
            # Calculate the mesh of x and y coordinates
            x = (np.array(range(self.x_size)) - center[0])*self.scale
            y = (np.array(range(self.y_size)) - center[1])*self.scale
            xs, ys = np.meshgrid(x, y)
            # Slit amplitude mask.
            T = np.where(abs(xs*np.cos(rotation) - ys*np.sin(rotation))
                         < width/2, 1, 0)
            self.array = self.array * T
            return None


class PolarizationLightField:
    pass
