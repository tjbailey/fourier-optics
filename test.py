import ftoptics as fo
import numpy as np
import matplotlib.pyplot as plt
import time

size = 10000

x = np.linspace(-1,1,size)
y = np.linspace(-1,1,size)
xx, yy = np.meshgrid(x,y)
print('generated mesh')
time.sleep(2)

E = np.exp(-xx**2)*np.exp(-yy**2)
print('generated emask')
time.sleep(2)
E = fo.LightField(E, scale=1e-4)
print('generated class')
time.sleep(2)
E.propagate(10, copy=False, low_memory=True)

plt.imshow(abs(E.array))
plt.show()
plt.imshow(abs(E2.array))
plt.show()
